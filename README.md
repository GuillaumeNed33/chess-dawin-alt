Object Oriented Programming - LP DAWIN
----------------------------------------
##C++ Chess
to discover or rediscover OOP, we had to do an chess game.

Basically we were supposed to do a terminal version, but we decided to upgrade 
the project and we worked on a graphical version. So we used [SFML 2.1](https://www.sfml-dev.org/index-fr.php)
and C++11. 

##Team
* Guillaume NEDELEC
* Alicia GASTON
* Thomas BLANC

##Functionalites 

* Console version
* Graphical version with SFML
* Installation of the chessboard and pieces
* Movement of pieces according to type (with visibility of the boxes where movements are possible )
* "In check" case
* PAT configuration
* Draw ending configuration (Only kings are still alive.)
* Checkmate configuration
* Possibility to run the game with a PAT, Draw or CheckMate configuration for tests (works only in console version)

##Possible improvements
* Allow the "roque" movement (simultaneous relocation of the tower and king)
* Allow the "promotion" (When a pawn reaches the last row of the chessboard, it is transformed into another piece of its color. )