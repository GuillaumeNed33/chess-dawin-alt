#include <iostream>
#include "SFML/Graphics.hpp"
#include "View/View.h"
#include "Model/Piece.h"
#include "Model/JoueurBlanc.h"
#include "Model/JoueurNoir.h"
#include "Model/Model.h"
#include "Controller/Controller.h"

using namespace std;

const int WIDTH = 500;
const int HEIGHT = 700;

/**
 * @brief chessTerminal
 * Lance le jeu en version terminal
 */
void EchecTerminal(int version) {
    // Creation de joueur
    JoueurBlanc j1; //blanc

    //    cout << endl << " ---------------------------------------------------" << endl;

    JoueurNoir j2; //noir

    //Echiquier
    Echiquier e;

    //Mise en places
    j1.placerPieces(e);
    j2.placerPieces(e);

    string  message="";

    switch(version) {
        case 1 :
            break;
        case 2 :
            //Initialise les pieces de facon a ce que le roi soit en echec au tour suivant
            e.configEchecEtMat();
            message =  "Bouger la reine en (6,7)";
            break;
        case 3 :
            //Initialise les pieces de facon a faire un match nul (Roi Blanc mange le Pion noir)
            e.configNul();
            message = "Match nul, Le roi blanc mange le pion noir";
            break;
        case 4 :
            //Initialise les pieces de facon a ce que les blancs soient en PAT
            e.configPAT();
            message = "Bouger le pion blanc de (7,3) en (7,4) et le Pion noir de (7,7) en (7,6)";
            break;
        default :
            cerr <<"switch(version) de EchecTerminal à reçu une valeur inattendue"<<endl;
            break;
    }

    //variable de jeu
    bool enJeu = true;
    int nbDeTours = 1; //nb tours
    bool tourDesBlancs = true;
    string consigne ="";
    int x,y, nextX, nextY;
    cout << "---------------------------------------------------------------" << endl;
    cout << "--------------------- DEBUT DE LA PARTIE ----------------------" << endl;
    cout << "---------------------------------------------------------------" << endl;
    while (enJeu) {

        cout << "---------------------------------------------------------------" << endl;
        cout << "---------------------------  TOUR " << nbDeTours << " ---------------------------" << endl;
        cout << "---------------------------------------------------------------" << endl;
        if(version != 1){
            cout<< message<<endl;
        }

        e.affiche();

        if(nbDeTours%2 == 1) {
            consigne = "AUX BLANCS DE JOUER !";
            tourDesBlancs = true;
        }
        else {
            consigne = "AUX NOIRS DE JOUER !";
            tourDesBlancs = false;
        }

        cout << consigne << endl;

        bool pieceValide = false;
        bool positionValide = false;

        while(!positionValide) {
            cout << "bouger la piece en x : ";
            cin >> x;
            cout << "bouger la piece en y : ";
            cin >> y;

            if(e.getPiece(x,y) != 0) {
                if(e.getPiece(x, y)->estBlanc() == tourDesBlancs) {
                    pieceValide = true;
                }
                else {
                    cout << "--- ERROR LA PIECE N'EST PAS A VOUS ! ---" << endl;
                }
            }
            else {
                cout << "--- ERROR CASE VIDE---" << endl;
            }

            if(pieceValide) {

                cout << "vers la position ( en x ) : ";
                cin >> nextX;
                cout << "vers la position ( en y ) : ";
                cin >> nextY;

                if (e.deplacer(e.getPiece(x,y), nextX, nextY)) {
                    positionValide = true;
                }
                else {
                    if(e.isCheck(e.getPiece(x,y), nextX, nextY)) {
                        cout << "--- VOUS ETES EN ECHEC ---" << endl;
                        pieceValide = false;
                    }
                    else {
                        cout << "--- IMPOSSIBLE ---" << endl;
                        pieceValide = false;
                    }

                }
            }
        }


        if(e.EchecEtMat(!tourDesBlancs)) {

            enJeu= false;
            cout << "---------------------------------------------------------------" << endl;
            cout << "-----------------------  ECHECS ET MAT ------------------------" << endl;
            cout << "---------------------------------------------------------------" << endl;

            e.affiche();

            if(nbDeTours%2 == 1) {
                cout << "---------------------------------------------------------------" << endl;
                cout << "-------------------  VICTOIRE DES BLANCS  ---------------------" << endl;
                cout << "---------------------------------------------------------------" << endl;
            }
            else {
                cout << "---------------------------------------------------------------" << endl;
                cout << "-------------------  VICTOIRE DES NOIRS  ----------------------" << endl;
                cout << "---------------------------------------------------------------" << endl;
            }
        }

        else if(e.PAT(!tourDesBlancs)) {

            enJeu= false;
            cout << "---------------------------------------------------------------" << endl;
            cout << "-----------------------  PAT DETECTE ! -----------------------" << endl;
            cout << "---------------------------------------------------------------" << endl;
            cout << "---------------------------------------------------------------" << endl;

            e.affiche();

            cout << "---------------------------------------------------------------" << endl;
            cout << "---------------------  MATCH NUL : PAT  -----------------------" << endl;
            if(nbDeTours%2 == 1) {
                cout << "------------- (les noirs ne peuvent plus bouger) --------------" << endl;
            } else {
                cout << "------------ (les blancs ne peuvent plus bouger) --------------" << endl;
            }
            cout << "---------------------------------------------------------------" << endl;
        }

        else if(e.isNul()) {
            enJeu= false;

            e.affiche();

            cout << "---------------------------------------------------------------" << endl;
            cout << "------------------------  MATCH NUL  -----------------------" << endl;
            cout << "--------------- (seul les rois ont survécus) ------------------" << endl;
            cout << "---------------------------------------------------------------" << endl;

        }

        else if(e.echec(!tourDesBlancs)) {
            if(nbDeTours%2 == 1) {
                cout << "---------------------------------------------------------------" << endl;
                cout << "-----------------------  NOIR EN ECHECS  ----------------------" << endl;
                cout << "---------------------------------------------------------------" << endl;
            }
            else {
                cout << "---------------------------------------------------------------" << endl;
                cout << "-----------------------  BLANC EN ECHECS  ---------------------" << endl;
                cout << "---------------------------------------------------------------" << endl;
            }
        }

        nbDeTours++;
    }
}


void launchSFML(int version) {
    View *v = new View(WIDTH,HEIGHT);
    Model *m = new Model();
    Controller c(v,m);
    c.gameLoop(version);

    delete m;
    delete v;
}

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
    cout << "---------------------------------------------------------------" << endl;
    cout << "--------------  BIENVENUE | CHOISISSEZ UNE VERSION  -----------" << endl;
    cout << "---------------------------------------------------------------" << endl;
    cout << "1. Version console" << endl;
    cout << "2. Version graphique" << endl;
    string input;
    bool selectionne = false;
    bool terminalVersion=false;
    while(!selectionne) {
        cout << "Version : "; cin >> input;
        if(input == "1") {
            selectionne = true;
            terminalVersion = true;
        }
        else if (input == "2") {
            selectionne = true;
        }
        else {
            cout << "Mauvaise entree, entrez 1 ou 2." << endl;
        }
    }
        cout << "1. Jouer" <<endl;
        cout << "2. Configuration d'echecs et mat"<<endl;
        cout <<"3. Configuration match nul"<<endl;
        cout <<"4. Configuration PAT" <<endl;

    selectionne = false;

    while(!selectionne){
        cout << "Mode :"; cin >>input;

        if(input == "1"){
            selectionne = true;
            if(terminalVersion) {
                EchecTerminal(1);
            }
            else
            {
                launchSFML(1);
            }
        }
        else if(input =="2"){
            selectionne = true;
            if(terminalVersion){
                EchecTerminal(2);
            }
            else{
                //config echec et mat SFML
                launchSFML(2);
            }
        }
        else if(input =="3"){
            selectionne = true;
            if(terminalVersion){
                EchecTerminal(3);
            }
            else{
                //config null SFML
                launchSFML(3);
            }
        }
        else if(input =="4"){
            selectionne = true;
            if(terminalVersion){
                EchecTerminal(4);
            }
            else{
                //config PAT SFML
                launchSFML(4);
            }
        }
        else {
            cout << "Mauvaise entree, entrez 1, 2, 3 ou 4." << endl;
        }
    }
    return 0;
}
