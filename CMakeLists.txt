cmake_minimum_required(VERSION 3.5)
project(POO_DAWIN_ALT)

set(CMAKE_CXX_STANDARD 11)

set(MODEL_FILES
        Model/Cavalier.h
        Model/Cavalier.cpp
        Model/Echiquier.h
        Model/Echiquier.cxx
        Model/Fou.cpp
        Model/Fou.h
        Model/Joueur.h
        Model/Joueur.cxx
        Model/JoueurBlanc.h
        Model/JoueurNoir.h
        Model/JoueurBlanc.cpp
        Model/JoueurNoir.cpp
        Model/Piece.cxx
        Model/Piece.h
        Model/Pion.cpp
        Model/Pion.h
        Model/Reine.cpp
        Model/Reine.h
        Model/Roi.cpp
        Model/Roi.h
        Model/Tour.cpp
        Model/Tour.h
        Model/Model.cpp Model/Model.h)

set(VIEW_FILES
        View/View.h
        View/View.cpp View/GraphicElement.cpp View/GraphicElement.h)

set(SOURCE_FILES
        ${MODEL_FILES}
        ${VIEW_FILES}
        Controller/Controller.cpp
        Controller/Controller.h
        main.cpp)

add_executable(POO_DAWIN_ALT ${SOURCE_FILES})

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(SFML 2 REQUIRED system window graphics network audio)
if(SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(POO_DAWIN_ALT ${SFML_LIBRARIES})
endif()