#include <iostream>
#include "View.h"
#include "GraphicElement.h"

using namespace std;
/**
 * View Constructor
 * @param width of the window
 * @param height of the window
 */
View::View(unsigned int width, unsigned int height) : m_width(width), m_height(height) {
    m_window = new sf::RenderWindow(sf::VideoMode(m_width,m_height,32),"Chess", sf::Style::Titlebar | sf::Style::Close);

    m_pieces.push_back(&m_whitePieces);
    m_pieces.push_back(&m_blackPieces);
    m_pieces.push_back(&m_whiteGraveward);
    m_pieces.push_back(&m_blackGraveward);

    initChessboard();
    initText();
}

/***
 * create rectangle for the chessboard
 */
void View::initChessboard() {
    bool white = true;
    for(int i = 0; i<8;i++) {
        for(int j = 0; j<8;j++) {
            sf::RectangleShape* rect = new sf::RectangleShape(sf::Vector2f(CASE_SIZE-3,CASE_SIZE-3));
            rect->setPosition(j*CASE_SIZE, i*CASE_SIZE+100);
            rect->setOutlineThickness(1);
            rect->setOutlineColor(sf::Color(128, 128, 128,255));
            rect->setFillColor(white?sf::Color(249, 249, 249, 255) : sf::Color(128, 128, 128,255));
            m_chessboard.push_back(rect);
            white = !white;
        }
        white = !white;
    }
}

/**
 * init font and graphical text for the game
 */
void View::initText() {
    if(m_font.loadFromFile(FONT_PATH)) {
        m_j1txt.setFont(m_font);
        m_j2txt.setFont(m_font);
        m_j1txt.setCharacterSize(FONT_SIZE);
        m_j2txt.setCharacterSize(FONT_SIZE);
        m_j1txt.setString(MSG_J1);
        m_j2txt.setString(MSG_J2);
        m_j1txt.setColor(sf::Color::White);
        m_j2txt.setColor(sf::Color::White);
        m_j1txt.setOrigin(m_j1txt.getLocalBounds().width/2, m_j1txt.getLocalBounds().height);
        m_j2txt.setOrigin(m_j2txt.getLocalBounds().width/2, m_j2txt.getLocalBounds().height);
        m_j1txt.setPosition(PADDING_TXT*2, PADDING_TXT);
        m_j2txt.setPosition(m_width - PADDING_TXT*2, m_height - PADDING_TXT);

        m_echec.setFont(m_font);
        m_echec.setCharacterSize(FONT_SIZE);
        m_echec.setString(' ');
        m_echec.setColor(sf::Color::White);
        m_echec.setOrigin(m_j1txt.getLocalBounds().width/2, m_j1txt.getLocalBounds().height);
        m_echec.setPosition(m_width - PADDING_TXT, PADDING_TXT);

        m_end_msg.setFont(m_font);
        m_end_msg.setCharacterSize(FONT_SIZE*2);
        m_end_msg.setString(' ');
        m_end_msg.setColor(sf::Color::Red);
        m_end_msg.setPosition(PADDING_TXT*2+18, PADDING_TXT*4+65);

        m_end_msg_info.setFont(m_font);
        m_end_msg_info.setCharacterSize(FONT_SIZE-5);
        m_end_msg_info.setString(' ');
        m_end_msg_info.setColor(sf::Color::Red);
        m_end_msg_info.setPosition(PADDING_TXT*3, PADDING_TXT*7);
    }
}

/**
 * Draw and display each graphic elements
 * @param loop int which represent turns
 */
void View::draw(int loop) {
    m_window->clear(sf::Color::Black);
    for(const sf::RectangleShape *r : m_chessboard) {
        m_window->draw(*r);
    }

    for(vector<GraphicElement*> * v : m_pieces) {
        for(GraphicElement* g : *v) {
            m_window->draw(*g);
        }
    }

    if(loop%2 == 1) {
        m_window->draw(m_j1txt);
    }
    else {
        m_window->draw(m_j2txt);
    }

    m_window->draw(m_echec);
    m_window->draw(m_end_msg);
    m_window->draw(m_end_msg_info);

    m_window->display();
}

/**
 * View destructor, delete elements in memory
 */
View::~View() {

    for(vector<GraphicElement*> * v : m_pieces) {
        for(GraphicElement* g : *v) {
            delete g;
        }
        v->clear();
    }
    m_pieces.clear();

    for(sf::RectangleShape * r : m_chessboard) {
        delete r;
    }
    m_chessboard.clear();

    delete m_window;

}

/**
 * get Window object
 *
 * @return
 */
sf::RenderWindow *View::getWindow() const {
    return m_window;
}

/**
 * put every graphic piece elements in the right position
 *
 */
void View::initChessPieces() {

    //Pions
    for(int i = 0; i < 8; i++) {
        m_blackPieces.push_back(new GraphicElement("PionNoir", i*CASE_SIZE, m_height -(96+CASE_SIZE*2),i+1,7));
        m_whitePieces.push_back(new GraphicElement("PionBlanc",i*CASE_SIZE, 100+CASE_SIZE, i+1, 2));
    }

    //Pieces specialisées blanches
    m_whitePieces.push_back(new GraphicElement("TourBlanc",0*CASE_SIZE, 100,1,1));
    m_whitePieces.push_back(new GraphicElement("CavalierBlanc",1*CASE_SIZE, 100,2,1));
    m_whitePieces.push_back(new GraphicElement("FouBlanc",2*CASE_SIZE, 100,3,1));
    m_whitePieces.push_back(new GraphicElement("ReineBlanc",3*CASE_SIZE, 100,4,1));
    m_whitePieces.push_back(new GraphicElement("RoiBlanc",4*CASE_SIZE, 100,5,1));
    m_whitePieces.push_back(new GraphicElement("FouBlanc",5*CASE_SIZE, 100,6,1));
    m_whitePieces.push_back(new GraphicElement("CavalierBlanc",6*CASE_SIZE, 100,7,1));
    m_whitePieces.push_back(new GraphicElement("TourBlanc",7*CASE_SIZE, 100,8,1));

    //Pieces specialisées noires
    m_blackPieces.push_back(new GraphicElement("TourNoir",0*CASE_SIZE, m_height -(96+CASE_SIZE),1,8));
    m_blackPieces.push_back(new GraphicElement("CavalierNoir",1*CASE_SIZE, m_height -(96+CASE_SIZE),2,8));
    m_blackPieces.push_back(new GraphicElement("FouNoir",2*CASE_SIZE, m_height -(96+CASE_SIZE),3,8));
    m_blackPieces.push_back(new GraphicElement("ReineNoir",3*CASE_SIZE, m_height -(96+CASE_SIZE),4,8));
    m_blackPieces.push_back(new GraphicElement("RoiNoir",4*CASE_SIZE, m_height -(96+CASE_SIZE),5,8));
    m_blackPieces.push_back(new GraphicElement("FouNoir",5*CASE_SIZE, m_height -(96+CASE_SIZE),6,8));
    m_blackPieces.push_back(new GraphicElement("CavalierNoir",6*CASE_SIZE, m_height -(96+CASE_SIZE),7,8));
    m_blackPieces.push_back(new GraphicElement("TourNoir",7*CASE_SIZE, m_height -(96+CASE_SIZE),8,8));
}

/**
 * Delete chessboard and piece and replace it
 */
void View::resetPositionValides() {
    for(sf::RectangleShape* r : m_chessboard) {
        delete r;
    }
    m_chessboard.clear();
    initChessboard();

}

/**
 * draw rect where the piece can moove
 * @param positions
 */
void View::afficherPositionValides(std::vector<std::pair<int, int> > positions) {

    for(std::pair<int, int> e : positions) {
        int x = e.first;
        int y = e.second;
        int res = ((y-1)*8)+x-1;
        m_chessboard.at(res)->setOutlineColor(sf::Color(225, 225, 225,110));
        m_chessboard.at(res)->setFillColor(sf::Color(110, 110, 110,110));
        m_chessboard.at(res)->setOutlineThickness(3);
    }
}

/**
 * moove pieces
 * @param posX
 * @param posY
 * @param destX
 * @param destY
 */
void View::moveSprite(int posX, int posY, int destX, int destY) {
    GraphicElement* selected = nullptr;
    GraphicElement* destruct = nullptr;

    int cpt = 0;
    for(GraphicElement *g : m_whitePieces) {
        if(g->getCaseX() == posX && g->getCaseY()== posY)
            selected = g;
        if(g->getCaseX() == destX && g->getCaseY()== destY) {
            destruct = g;
            destruct->setPositionInGrid(-100,-100);
            destruct->setCaseX(-1);
            destruct->setCaseY(-1);
            m_whitePieces.erase(m_whitePieces.begin()+cpt);
            m_whitePieces.push_back(destruct);

        }
        cpt++;
    }

    cpt = 0;

    for(GraphicElement *g : m_blackPieces) {
        if(g->getCaseX() == posX && g->getCaseY()== posY)
            selected = g;
        if(g->getCaseX() == destX && g->getCaseY()== destY) {
            destruct = g;
            destruct->setPositionInGrid(-100,-100);
            destruct->setCaseX(-1);
            destruct->setCaseY(-1);
            m_blackPieces.erase(m_blackPieces.begin()+cpt);
            m_blackGraveward.push_back(destruct);
        }
        cpt++;
    }

    selected->setPositionInGrid((destX-1)*CASE_SIZE, 100+((destY-1)*CASE_SIZE));
    selected->setCaseY(destY);
    selected->setCaseX(destX);
}

/**
 * moove the check info message in the right position
 *
 *@param white boolean representing if the message is for white or black player
 */
void View::setCheckInfo(bool white) {
    m_echec.setString(ECHEC_MSG);
    if(!white) {
        m_echec.setOrigin(m_j2txt.getLocalBounds().width / 2, m_j2txt.getLocalBounds().height);
        m_echec.setPosition(PADDING_TXT * 2, m_height - PADDING_TXT);
    }
    else {
        m_echec.setOrigin(m_j1txt.getLocalBounds().width / 2, m_j1txt.getLocalBounds().height);
        m_echec.setPosition(m_width - PADDING_TXT, PADDING_TXT);
    }
}


/**
 * reset check message when there's nothing to display
 *
 */
void View::resetCheckInfo() {
    m_echec.setString(' ');
}


void View::setEndGameMsg(int idEnd, bool white) {
    switch (idEnd) {
        case 0 :
            m_end_msg.setString(NUL_MSG);
            m_end_msg_info.setString(QUIT_MSG);
            break;
        case 1:
            m_end_msg.setString(MAT_MSG);
            if(!white)
                m_end_msg_info.setString(WIN_WHITE +ENDL+ QUIT_MSG);
            else
                m_end_msg_info.setString(WIN_BLACK +ENDL + QUIT_MSG);
            break;
        case 2 :
            m_end_msg.setString(PAT_MSG);
            m_end_msg_info.setString(QUIT_MSG);
            break;
        default:
            m_end_msg.setString(NUL_MSG);
            m_end_msg_info.setString(QUIT_MSG);
            break;
    }
}

void View::setSpecialConfig(int type) {
    if(type == 0) //config match nul {
        initPiecesNul();
    else if(type==1) {
        initPiecesPAT(); //configuration du pat
    }
    else if(type == 2) {
        initPiecesMAT(); //configuration echec et mat
    }
}

void View::initPiecesNul() {

    for(GraphicElement* g : m_blackPieces)
        delete g;
    for(GraphicElement* g : m_whitePieces)
        delete g;

    m_blackPieces.clear();
    m_whitePieces.clear();

    m_whitePieces.push_back(new GraphicElement("RoiBlanc",1*CASE_SIZE, 100+(3*CASE_SIZE),2,4));
    m_blackPieces.push_back(new GraphicElement("RoiNoir",5*CASE_SIZE, 100+(6*CASE_SIZE),6,7));
    m_blackPieces.push_back(new GraphicElement("PionNoir",2*CASE_SIZE, 100+(3*CASE_SIZE),3,4));

}

void View::initPiecesPAT() {
    for(GraphicElement* g : m_blackPieces)
        delete g;
    for(GraphicElement* g : m_whitePieces)
        delete g;

    m_blackPieces.clear();
    m_whitePieces.clear();

    m_whitePieces.push_back(new GraphicElement("PionBlanc",6*CASE_SIZE, 100+(4*CASE_SIZE),7,5));
    m_whitePieces.push_back(new GraphicElement("PionBlanc",6*CASE_SIZE, 100+(2*CASE_SIZE),7,3));
    m_whitePieces.push_back(new GraphicElement("FouBlanc",7*CASE_SIZE, 100+(2*CASE_SIZE),8,3));
    m_whitePieces.push_back(new GraphicElement("RoiBlanc",7*CASE_SIZE, 100+(3*CASE_SIZE),8,4));

    m_blackPieces.push_back(new GraphicElement("CavalierNoir",4*CASE_SIZE, 100+(3*CASE_SIZE),5,4));
    m_blackPieces.push_back(new GraphicElement("TourNoir",7*CASE_SIZE, 100+(0*CASE_SIZE),8,1));
    m_blackPieces.push_back(new GraphicElement("RoiNoir",5*CASE_SIZE, 100+(2*CASE_SIZE),6,3));
    m_blackPieces.push_back(new GraphicElement("PionNoir",6*CASE_SIZE, 100+(6*CASE_SIZE),7,7));
}

void View::initPiecesMAT() {

    for(GraphicElement* g : m_blackPieces)
        delete g;
    for(GraphicElement* g : m_whitePieces)
        delete g;

    m_blackPieces.clear();
    m_whitePieces.clear();

    //Pions
    for(int i = 0; i < 8; i++) {
        m_blackPieces.push_back(new GraphicElement("PionNoir", i*CASE_SIZE, m_height -(96+CASE_SIZE*2),i+1,7));
        m_whitePieces.push_back(new GraphicElement("PionBlanc",i*CASE_SIZE, 100+CASE_SIZE, i+1, 2));
    }

    //Pieces specialisées blanches
    m_whitePieces.push_back(new GraphicElement("TourBlanc",0*CASE_SIZE, 100,1,1));
    m_whitePieces.push_back(new GraphicElement("CavalierBlanc",1*CASE_SIZE, 100,2,1));
    m_whitePieces.push_back(new GraphicElement("FouBlanc",2*CASE_SIZE, 100,3,1));
    m_whitePieces.push_back(new GraphicElement("ReineBlanc",7*CASE_SIZE, 100+(4*CASE_SIZE),8,5));
    m_whitePieces.push_back(new GraphicElement("RoiBlanc",4*CASE_SIZE, 100,5,1));
    m_whitePieces.push_back(new GraphicElement("FouBlanc",2*CASE_SIZE, 100+(3*CASE_SIZE),3,4));
    m_whitePieces.push_back(new GraphicElement("CavalierBlanc",6*CASE_SIZE, 100,7,1));
    m_whitePieces.push_back(new GraphicElement("TourBlanc",7*CASE_SIZE, 100,8,1));

    //Pieces specialisées noires
    m_blackPieces.push_back(new GraphicElement("TourNoir",0*CASE_SIZE, m_height -(96+CASE_SIZE),1,8));
    m_blackPieces.push_back(new GraphicElement("CavalierNoir",1*CASE_SIZE, m_height -(96+CASE_SIZE),2,8));
    m_blackPieces.push_back(new GraphicElement("FouNoir",2*CASE_SIZE, m_height -(96+CASE_SIZE),3,8));
    m_blackPieces.push_back(new GraphicElement("ReineNoir",3*CASE_SIZE, m_height -(96+CASE_SIZE),4,8));
    m_blackPieces.push_back(new GraphicElement("RoiNoir",4*CASE_SIZE, m_height -(96+CASE_SIZE),5,8));
    m_blackPieces.push_back(new GraphicElement("FouNoir",5*CASE_SIZE, m_height -(96+CASE_SIZE),6,8));
    m_blackPieces.push_back(new GraphicElement("CavalierNoir",6*CASE_SIZE, m_height -(96+CASE_SIZE),7,8));
    m_blackPieces.push_back(new GraphicElement("TourNoir",7*CASE_SIZE, m_height -(96+CASE_SIZE),8,8));
}