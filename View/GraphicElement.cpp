#include "GraphicElement.h"

using namespace std;

/**
 * GraphicElement Constructor, set the texture, and position.
 * @param fileName string allowing the texture to be loaded
 * @param x position x of the GraphicElement
 * @param y position y of the GraphicElement
 * @param caseX position x on the grid
 * @param caseY posittion y on the grid
 */
GraphicElement::GraphicElement(string fileName, int x, int y, int caseX, int caseY) : m_x{x}, m_y{y}, m_caseX{caseX}, m_caseY{caseY} {
    if(m_texture.loadFromFile(IMG_DIR + fileName + IMG_TYPE)) {
        this->setTexture(m_texture);
    }
    this->setPosition(m_x, m_y);
}

/**
 * get position x on the grid
 *
 * @return
 */
int GraphicElement::getCaseX() const {
    return m_caseX;
}
 /**
  * get position y on the grid
  *
  * @return
  */
int GraphicElement::getCaseY() const {
    return m_caseY;
}

/**
 * set position x on the grid
 *
 * @param x
 */
void GraphicElement::setCaseX(int x) {
    m_caseX=x;
}

/**
 * set position y on the grid
 *
 * @param y
 */
void GraphicElement::setCaseY(int y) {
    m_caseY = y;
}

/**
 * set position in the graphical window
 * @param x left
 * @param y top
 */
void GraphicElement::setPositionInGrid(int x, int y) {
    m_x=x;
    m_y=y;
    this->setPosition(m_x, m_y);
}
