#ifndef POO_DAWIN_ALT_VIEW_H
#define POO_DAWIN_ALT_VIEW_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include "GraphicElement.h"
#include "../Model/Piece.h"

const std::string FONT_PATH="../Assets/Font/chesterfield.ttf";
const int FONT_SIZE = 40;
const int PADDING_TXT = 50;
const std::string MSG_J1 = "PLAYER 1 : TURN";
const std::string MSG_J2 = "PLAYER 2 : TURN";
const std::string ECHEC_MSG = "ECHEC !";
const std::string MAT_MSG = "ECHEC ET MAT !";
const std::string PAT_MSG = "PAT : MATCH NUL !";
const std::string NUL_MSG = "MATCH NUL !";
const std::string QUIT_MSG = "APPUYER SUR ENTREE POUR QUITTER.";
const std::string WIN_WHITE = "LES BLANCS GAGNENT ";
const std::string WIN_BLACK = "LES NOIRS GAGNENT !";
const std::string ENDL = "\n";


const float CASE_SIZE = 500/8 + 1;

class View {
private :
    unsigned int m_width;
    unsigned int m_height;
    sf::RenderWindow *m_window;
    std::vector<sf::RectangleShape*> m_chessboard;
    //sf::RectangleShape* m_chessboard[64];

    std::vector<std::vector<GraphicElement*> *> m_pieces;
    std::vector<GraphicElement*> m_whitePieces;
    std::vector<GraphicElement*> m_blackPieces;
    std::vector<GraphicElement*> m_whiteGraveward;
    std::vector<GraphicElement*> m_blackGraveward;

    sf::Font m_font;
    sf::Text m_j1txt;
    sf::Text m_j2txt;

    sf::Text m_echec;
    sf::Text m_end_msg;
    sf::Text m_end_msg_info;


    void initChessboard();
    void initText();

public :
    View(unsigned int width, unsigned int height);
    void draw(int loop);
    sf::RenderWindow *getWindow() const;
    ~View();
    void initChessPieces();
    void resetPositionValides();
    void afficherPositionValides(std::vector<std::pair<int, int> > positions);
    void moveSprite(int posX, int posY, int destX, int destY);
    void setCheckInfo(bool white);
    void resetCheckInfo();
    void setEndGameMsg(int idEnd, bool white);

    void setSpecialConfig(int type);
    void initPiecesNul();
    void initPiecesPAT();
    void initPiecesMAT();
};



#endif //POO_DAWIN_ALT_VIEW_H
