#ifndef POO_DAWIN_ALT_GRAPHICELEMENT_H
#define POO_DAWIN_ALT_GRAPHICELEMENT_H


#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

const std::string IMG_DIR = "../Assets/Sprite/";
const std::string IMG_TYPE = ".png";

class GraphicElement : public sf::Sprite {
private:
    sf::Texture m_texture;
    int m_x;
    int m_y;
    int m_caseX;
    int m_caseY;
public:
    GraphicElement(std::string fileName, int x, int y, int caseX, int caseY);
    int getCaseX() const;
    int getCaseY() const;
    void setCaseX(int x);
    void setCaseY(int y);
    void setPositionInGrid(int x, int y);

};


#endif //POO_DAWIN_ALT_GRAPHICELEMENT_H
