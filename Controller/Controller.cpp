#include "Controller.h"
#include <iostream>
using namespace std;

/**
 * Controller Constructor
 *
 * @param view graphic manager class
 * @param model logic manager class
 */
Controller::Controller(View *view, Model *model) {
    m_view = view;
    m_model = model;
    m_view->initChessPieces();
}

/**
 * Manage menu and game
 *
 * @param version
 */
void Controller::gameLoop(int version) {
    if(version == 2) {
        m_model->getEchiquier().configEchecEtMat();
        m_view->initPiecesMAT();
    }
    else if(version == 3) {
        m_model->getEchiquier().configNul();
        m_view->initPiecesNul();
    }
    else if(version == 4) {
        m_model->getEchiquier().configPAT();
        m_view->initPiecesPAT();
    }

    int loop=1;
    std::vector<std::pair<int, int> > casesPossibles;
    int pieceSelectX = 0;
    int pieceSelectY = 0;
    bool endGame = false;

    //Main Loop
    while (m_view->getWindow()->isOpen()) {

        bool whiteTurn = loop % 2 == 1;

        if(m_model->getEchiquier().isNul()) {
            m_view->setEndGameMsg(0, whiteTurn);
            endGame = true;
        }
        else if(m_model->getEchiquier().EchecEtMat(whiteTurn)) {
            m_view->setEndGameMsg(1, whiteTurn);
            endGame = true;
        }
        else if(m_model->getEchiquier().PAT(whiteTurn)) {
            m_view->setEndGameMsg(2, whiteTurn);
            endGame = true;
        }


        //Event Loop
        while (m_view->getWindow()->pollEvent(m_event)) {
            if ((m_event.type == sf::Event::Closed)  || ((m_event.type == sf::Event::KeyPressed) && (m_event.key.code == sf::Keyboard::Escape))) {
                m_view->getWindow()->close();
            }

            //Si le jeu est en cours
            if(!endGame) {
                if (m_event.type == sf::Event::MouseButtonPressed && m_event.mouseButton.button == sf::Mouse::Left) {
                    sf::Vector2i localPosition = sf::Mouse::getPosition(
                            *m_view->getWindow()); //Position de la souris dans la fenetre

                    if (localPosition.y > 100 && localPosition.y < 605) {
                        int caseX = ((localPosition.x) / (int) CASE_SIZE) + 1; //CASE X DE LA VUE
                        int caseY = ((localPosition.y - 100) / ((int) CASE_SIZE)) + 1; //CASE Y DE LA VUE

                        //TOUR DES BLANCS
                        if (whiteTurn) {
                            if (m_model->getEchiquier().getPiece(caseX, caseY) != 0 &&
                                    m_model->getEchiquier().getPiece(caseX, caseY)->estBlanc()) {
                                m_view->resetPositionValides(); //reset les couleurs de l'echiquier

                                pieceSelectX = caseX;
                                pieceSelectY = caseY;

                                casesPossibles.clear();
                                casesPossibles = m_model->getPossibilities(caseX, caseY);

                                m_view->afficherPositionValides(
                                        casesPossibles); //surbrillance des cases de deplacements possibles
                            } else if (pieceSelectX != 0 && pieceSelectY != 0 &&
                                       !casesPossibles.empty()) { //piece selectionne et clic sur case compatible avec deplacement
                                bool movable = false;
                                for (std::pair<int, int> p : casesPossibles) {
                                    if (p.first == caseX && p.second == caseY)
                                        movable = true;
                                }
                                if (movable) {
                                    m_view->resetPositionValides(); //reset les couleurs de l'echiquier
                                    if (m_model->deplacer(pieceSelectX, pieceSelectY, caseX, caseY)) {
                                        m_view->moveSprite(pieceSelectX, pieceSelectY, caseX, caseY);
                                        pieceSelectX = 0;
                                        pieceSelectY = 0;
                                        loop++;
                                        m_view->resetCheckInfo();
                                        if (m_model->getEchiquier().echec(!whiteTurn)) {
                                            m_view->setCheckInfo(!whiteTurn);
                                        }
                                    }
                                }
                            }
                        }

                            //TOUR DES NOIRS
                        else {
                            if (m_model->getEchiquier().getPiece(caseX, caseY) != 0 &&
                                    m_model->getEchiquier().getPiece(caseX, caseY)->estNoir()) {
                                m_view->resetPositionValides(); //reset les couleurs de l'echiquier

                                pieceSelectX = caseX;
                                pieceSelectY = caseY;

                                casesPossibles.clear();
                                casesPossibles = m_model->getPossibilities(caseX, caseY);

                                m_view->afficherPositionValides(
                                        casesPossibles); //surbrillance des cases de deplacements possibles
                            } else if (pieceSelectX != 0 && pieceSelectY != 0 && !casesPossibles.empty()) {
                                bool movable = false;
                                for (std::pair<int, int> p : casesPossibles) {
                                    if (p.first == caseX && p.second == caseY)
                                        movable = true;
                                }
                                if (movable) {
                                    m_view->resetPositionValides(); //reset les couleurs de l'echiquier
                                    if (m_model->deplacer(pieceSelectX, pieceSelectY, caseX, caseY)) {
                                        m_view->moveSprite(pieceSelectX, pieceSelectY, caseX, caseY);
                                        pieceSelectX = 0;
                                        pieceSelectY = 0;
                                        loop++;
                                        m_view->resetCheckInfo();
                                        if (m_model->getEchiquier().echec(!whiteTurn)) {
                                            m_view->setCheckInfo(!whiteTurn);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if((m_event.type == sf::Event::KeyPressed) && (m_event.key.code == sf::Keyboard::Return)) {
                    m_view->getWindow()->close();
                }
            }
        }
        //End Event Loop
        m_view->draw(loop);
    }
}

Model *Controller::getModel() const {
    return m_model;
}
