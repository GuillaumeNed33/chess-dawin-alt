
#ifndef POO_DAWIN_ALT_CONTROLLER_H
#define POO_DAWIN_ALT_CONTROLLER_H


#include <SFML/Window/Event.hpp>
#include "../View/View.h"
#include "../Model/Model.h"

class Controller {
private:
    View *m_view;
    Model *m_model;
    sf::Event m_event;
public:
    Controller(View *view, Model *model);
    void gameLoop(int version);
    Model* getModel() const;

};


#endif //POO_DAWIN_ALT_CONTROLLER_H
