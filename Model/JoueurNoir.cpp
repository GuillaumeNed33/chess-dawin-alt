#include "JoueurNoir.h"
#include <iostream>

using namespace std;

/**
 * @brief JoueurNoir::JoueurNoir
 */
JoueurNoir::JoueurNoir() : Joueur(false) {
}

/**
 * @brief JoueurNoir::~JoueurNoir
 */
JoueurNoir::~JoueurNoir()
{
   // cout << "Destruction de joueur noir " << endl;
}

/**
 * @brief JoueurNoir::isWhite
 * @return couleur du joueur (blanc)
 */
bool
JoueurNoir::estBlanc() const
{
    return false;
}

/**
 * @brief JoueurNoir::isBlack
 * @return couleur du joueur (noir)
 */
bool
JoueurNoir::estNoir() const
{
    return true;
}
