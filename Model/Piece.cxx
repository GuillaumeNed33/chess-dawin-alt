/** 
 * Mise en oeuvre de Piece.h
 *
 * @file Piece.cxx
 */

// A besoin de la declaration de la classe
#include "Piece.h"
#include <iostream>

using namespace std;
/**
 * @brief Piece::Piece
 */
Piece::Piece()
{
   // std::cout << "Nouvelle pièce (défaut)! " << std::endl;
    // ne fait rien => objet instancie mais non valide.
}

/**
 * @brief Piece::Piece
 * @param x
 * @param y
 * @param white
 * Création pièce paramétrée
 */
Piece::Piece(int x, int y, bool white ) : m_x(x), m_y(y), m_white(white)
{
   // std::cout << "Nouvelle pièce (paramétré)! " << std::endl;
}

/**
 * @brief Piece::~Piece
 */
Piece::~Piece() {
  //  std::cout << "Destruction pièce! " << std::endl;
}


/**
 * @brief Piece::Piece
 * @param autre
 * Constructeur par copie
 */
Piece::Piece(const Piece & autre)
{
    m_x=autre.m_x;
    m_y=autre.m_y;
    m_white=autre.m_white;
  //  std::cout << "Constructon Piece par copie" << endl;
}

/**
 * @brief Piece::operator =
 * @param autre
 * @return piece
 * Operateur d'affectation
 */
Piece &
Piece::operator=(const Piece & autre)
{
    m_x=autre.m_x;
    m_y=autre.m_y;
    m_white=autre.m_white;
 //   std::cout << "Operator affectation Piece" << endl;
    return *this;
}

/**
 * @brief Piece::init
 * @param x
 * @param y
 * @param white
 * Initialise les coordonnées et couleur d'une piece
 */
void
Piece::init( int x, int y, bool white )
{
    m_x = x;
    m_y = y;
    m_white = white;
}

/**
 * @brief Piece::move
 * @param x
 * @param y
 * Deplace la piece en x,y
 */
void
Piece::move( int x, int y )
{
    m_x = x;
    m_y = y;
}

/**
 * @brief Piece::x
 * @return coordonnees x
 */
int
Piece::x() const
{
    return m_x;
}

/**
 * @brief Piece::y
 * @return coordonnees y
 */
int
Piece::y() const
{
    return m_y;
}

/**
 * @brief Piece::isWhite
 * @return true si la piece est blanche
 */
bool
Piece::estBlanc() const
{
    return m_white;
}

/**
 * @brief Piece::isBlack
 * @return true si la piece est noire
 */
bool
Piece::estNoir() const
{
    return !m_white;
}

/**
 * @brief Piece::affiche
 * Affiche les coordonnes et couleur de la piece
 */
void
Piece::affiche() const {
    std::cout << "Piece: x=" << this->x() << " y=" << this->y() << " "
              << (this->estBlanc() ? "blanche" : "noire" ) << std::endl;
}

/**
 * @brief Piece::type
 * @return Representation graphique de la piece (terminal)
 */
char
Piece::type()
{
  return m_white ? 'B' : 'n';
}
