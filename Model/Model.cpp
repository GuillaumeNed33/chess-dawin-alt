#include "Model.h"

using namespace std;

Model::Model() : m_joueurBlanc{}, m_joueurNoir{}, m_echiquier{} {
    m_joueurBlanc.placerPieces(m_echiquier);
    m_joueurNoir.placerPieces(m_echiquier);
}

/**
 * Récupère la position des pieces de la couleur en parametre
 * @param white couleur du joueur
 * @return
 */
std::vector<std::pair<int, int>> Model::getPositions(bool white) {
    return white ? m_joueurBlanc.getPositions() : m_joueurNoir.getPositions();
}

/**
 * Recuperation de l'echiquier
 * @return m_echiquier
 */
Echiquier Model::getEchiquier() const {
    return m_echiquier;
}

/**
 *
 * @return le joueur blanc
 */
JoueurBlanc Model::getJoueurBlanc() const {
    return m_joueurBlanc;
}

/**
 *
 * @return joueur noir
 */
JoueurNoir Model::getJoueurNoir() const {
    return m_joueurNoir;
}

/**
 * Renvoie les positions de déplacement possible pour la piece se trouvant aux coordonnées en parametres
 * @param posX position X de la piece
 * @param posY position Y de la piece
 * @return l'ensemble des coordonnees possibles
 */
std::vector<std::pair<int, int> > Model::getPossibilities(int posX, int posY) {
    std::vector<std::pair<int, int> > res;
    for(int i=1; i<=8; i++){
        for(int j=1; j<=8; j++) {
            if(m_echiquier.getPiece(posX, posY)->mouvementValide(m_echiquier, j, i) &&
                    !m_echiquier.isCheck(m_echiquier.getPiece(posX, posY), j,i) ) {
                res.push_back(std::pair<int,int>(j,i));
            }
        }
    }
    return res;
}

/**
 * Effectue un deplacement
 * @param posX position X de départ
 * @param posY position Y de départ
 * @param destX position X d'arrivée
 * @param destY position Y d'arrivée
 * @return déplacement effectué
 */
bool Model::deplacer(int posX, int posY, int destX, int destY) {
    return m_echiquier.deplacer(m_echiquier.getPiece(posX,posY), destX, destY);
}
