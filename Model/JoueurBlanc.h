#ifndef JOUEURBLANC_H
#define JOUEURBLANC_H

#include "Joueur.h"

class JoueurBlanc : public Joueur {
public:
    JoueurBlanc();
    ~JoueurBlanc();

    bool estBlanc() const;
    bool estNoir() const;
};

#endif // JOUEURBLANC_H
