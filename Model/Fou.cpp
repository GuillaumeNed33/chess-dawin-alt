#include "Fou.h"
#include <iostream>
#include "Echiquier.h"
#include <stdlib.h>

using namespace std;

/**
 * @brief Fou::Fou
 */
Fou::Fou()
{
   // cout << "Nouveau Fou."<< endl;
}

/**
 * @brief Fou::Fou
 * @param white
 * @param gauche
 */
Fou::Fou(bool white, bool gauche) : Piece(gauche?3:6, white?1:8, white)
{
   // int y = (white)?1:8;
   // int x = (gauche)?3:6;

   // cout << "Nouveau Fou en ( " << x << " , " << y << " )."<< endl;
}

/**
 * @brief Fou::type
 * @return Representation graphique de la piece (terminal)
 */
char
Fou::type()
{
  return m_white ? 'F' : 'f';
}

/**
 * @brief Fou::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Fou::mouvementValide(Echiquier &e, int x, int y) {
    //Coordonnées valides
    if( (x >=1 && x <=8) && (y >=1 && y <=8))
    {
        //Blanc
        if(this->estBlanc()) {
            //Mouvement diagonale haut gauche
            if( (m_y-y == m_x-x) && (m_y-y > 0 && m_x-x > 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x-i, m_y-i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estNoir())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale haut droite
            if( (m_y-y == (m_x-x)*(-1)) && (m_y-y > 0 && m_x-x < 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x+i, m_y-i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estNoir())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale bas gauche
            if( (m_y-y == (m_x-x)*(-1)) && (m_y-y < 0 && m_x-x > 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x-i, m_y+i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estNoir())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale bas droite
            if( (m_y-y == m_x-x) && (m_y-y < 0 && m_x-x < 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x+i, m_y+i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estNoir())
                        valid = true;
                    i++;
                }
                return valid;
            }
        }

        //noir
        else {
            //Mouvement diagonale haut gauche
            if( (m_y-y == m_x-x) && (m_y-y > 0 && m_x-x > 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x-i, m_y-i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estBlanc())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale haut droite
            if( (m_y-y == (m_x-x)*(-1)) && (m_y-y > 0 && m_x-x < 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x+i, m_y-i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estBlanc())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale bas gauche
            if( (m_y-y == (m_x-x)*(-1)) && (m_y-y < 0 && m_x-x > 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x-i, m_y+i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estBlanc())
                        valid = true;
                    i++;
                }
                return valid;
            }

            //Mouvement diagonale bas droite
            if( (m_y-y == m_x-x) && (m_y-y < 0 && m_x-x < 0)) {
                int dist = abs(m_y-y);
                int i = 1;
                bool valid = true;
                while (i <= dist && valid) {
                    if(e.getPiece(m_x+i, m_y+i) != 0) {
                        valid = false;
                    }
                    if(i==dist && e.getPiece(x,y) != 0 && e.getPiece(x, y)->estBlanc())
                        valid = true;
                    i++;
                }
                return valid;
            }
        }
    }
    return false;
}
