#ifndef JOUEURNOIR_H
#define JOUEURNOIR_H

#include "Joueur.h"

class JoueurNoir : public Joueur {
public:
    JoueurNoir();
    ~JoueurNoir();

    bool estBlanc() const;
    bool estNoir() const;
};

#endif // JOUEURNOIR_H
