#include "Pion.h"
#include <iostream>
#include "Echiquier.h"

using namespace std;

/**
 * @brief Pion::Pion
 */
Pion::Pion()
{
   // cout << "Nouveau Pion."<< endl;
}

/**
 * @brief Pion::Pion
 * @param i
 * @param white
 */
Pion::Pion(int i, bool white) : Piece(i, white?2:7, white){
   // int y = (white)?2:7;
  //  cout << "Nouveau Pion en ( " << i << " , " << y << " )."<< endl;
}

/**
 * @brief Pion::type
 * @return Representation graphique de la piece (terminal)
 */
char
Pion::type()
{
  return m_white ? 'P' : 'p';
}

/**
 * @brief Pion::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Pion::mouvementValide(Echiquier &e, int x, int y) {

    //Coordonnées valides
    if( (x >=1 && x <=8) && (y >=1 && y <=8))
    {
        //Pion Blanc
        if(this->estBlanc()) {
            //Mouvement droit de 1
            if(m_y+1 == y && m_x == x && e.getPiece(x,y) == 0)
                return true;

            //Mouvement droit de 2
            else if(m_y == 2 && m_y+2 == y && m_x == x && e.getPiece(m_x, m_y+1) == 0 && e.getPiece(x, y) == 0)
                return true;

            //Mouvement en diagonale
            else if(e.getPiece(x, y) != 0 && e.getPiece(x, y)->estNoir() && (m_y+1 == y && (m_x-1 == x || m_x+1 == x)))
                return true;
        }
        //Pion noir
        else {
            //Mouvement droit de 1
            if(m_y-1 == y && m_x == x && e.getPiece(x,y) == 0)
                return true;

            //Mouvement droit de 2
            else if(m_y == 7 && m_y-2 == y && m_x == x && e.getPiece(m_x, m_y-1) == 0 && e.getPiece(x, y) == 0)
                return true;

            //Mouvement en diagonale
            else if(e.getPiece(x, y) != 0 && e.getPiece(x, y)->estBlanc() && (m_y-1 == y && (m_x-1 == x || m_x+1 == x)))
                return true;
        }
    }
    return false;
}
