#include "JoueurBlanc.h"
#include <iostream>

using namespace std;
/**
 * @brief JoueurBlanc::JoueurBlanc
 */
JoueurBlanc::JoueurBlanc() : Joueur(true) {
}

/**
 * @brief JoueurBlanc::~JoueurBlanc
 */
JoueurBlanc::~JoueurBlanc()
{
  //  cout << "Destruction de joueur blanc " << endl;
}

/**
 * @brief JoueurBlanc::isWhite
 * @return couleur du joueur
 */
bool
JoueurBlanc::estBlanc() const
{
    return true;
}

/**
 * @brief JoueurBlanc::isBlack
 * @return couleur du joueur
 */
bool
JoueurBlanc::estNoir() const
{
    return false;
}
