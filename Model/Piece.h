
/**
 * Header de Piece.cxx
 *
 * @file Piece.h
 */

#if !defined Piece_h
#define Piece_h

/**
 * Declaration d'une classe mod�lisant une piece de jeu d'echec.
 */



class Echiquier;

class Piece 
{
protected:
    int m_x;
    int m_y;
    bool m_white;

public:
    Piece();
    Piece( int x, int y, bool white );
    Piece(const Piece & autre);
    virtual ~Piece();
    Piece& operator=(const Piece & autre);

    void init( int x, int y, bool white );
    void move( int x, int y );
    int x() const;
    int y() const;
    bool estBlanc() const;
    bool estNoir() const;
    void affiche() const;
    virtual bool mouvementValide(Echiquier &e, int x, int y)=0;
    virtual char type();
}; 

#endif // !defined Piece_h
