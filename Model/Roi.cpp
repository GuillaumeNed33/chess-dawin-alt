#include "Roi.h"
#include <iostream>
#include "Echiquier.h"
#include <stdlib.h>

using namespace std;

/**
 * @brief Roi::Roi
 */
Roi::Roi()
{
   // cout << "Nouveau Roi."<< endl;
}

/**
 * @brief Roi::Roi
 * @param white
 */
Roi::Roi(bool white) : Piece(5, white?1:8, white){
   // int y = (white)?1:8;
  //  cout << "Nouveau Roi en ( " << 5 << " , " << y << " )."<< endl;
}

/**
 * @brief Roi::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Roi::mouvementValide(Echiquier &e, int x, int y) {
    //Coordonnées valides
    if( (x >=1 && x <=8) && (y >=1 && y <=8))
    {
        //Blanc
        if(this->estBlanc()) {
            if(abs(m_x-x) <= 1 && abs(m_y-y) <= 1 && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;
        }
        //noir
        else {
            if(abs(m_x-x) <= 1 && abs(m_y-y) <= 1 && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;
        }
    }
    return false;
}

/**
 * @brief Roi::type
 * @return Representation graphique de la piece (terminal)
 */
char
Roi::type()
{
    return m_white ? 'K' : 'k';
}
