#include "Cavalier.h"
#include <iostream>
#include "Echiquier.h"

using namespace std;

/**
 * @brief Cavalier::Cavalier
 */
Cavalier::Cavalier()
{
  //  cout << "Nouveau Cavalier."<< endl;
}

/**
 * @brief Cavalier::Cavalier
 * @param blanc
 * @param gauche
 */
Cavalier::Cavalier(bool blanc, bool gauche) : Piece(gauche?2:7, blanc?1:8, blanc)
{
    //int y = (blanc)?1:8;
    //int x = (gauche)?2:7;
   // cout << "Nouveau Cavalier en ( " << x << " , " << y << " )."<< endl;
}

/**
 * @brief Cavalier::type
 * @return Representation graphique de la piece (terminal)
 */
char
Cavalier::type()
{
  return m_white ? 'C' : 'c';
}

/**
 * @brief Cavalier::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Cavalier::mouvementValide(Echiquier &e, int x, int y) {

    //Coordonnées valides
    if( (x >=1 && x <=8) && (y >=1 && y <=8))
    {
        //Blanc
        if(this->estBlanc()) {
            //Mouvement 2haut 1Gauche
            if(m_y+2 == y && m_x-1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2haut 1Droite
            else if(m_y+2 == y && m_x+1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Gauche 1Haut
            else if(m_y+1 == y && m_x-2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Gauche 1Bas
            else if(m_y-1 == y && m_x-2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Bas 1Gauche
            else if(m_y-2 == y && m_x-1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Bas 1Droite
            else if(m_y-2 == y && m_x+1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Droite 1Haut
            else if(m_y+1 == y && m_x+2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;

            //Mouvement 2Droite 1Bas
            else if(m_y-1 == y && m_x+2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estNoir()))
                return true;
        }
        //noir
        else {
            //Mouvement 2haut 1Gauche
            if(m_y+2 == y && m_x-1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2haut 1Droite
            else if(m_y+2 == y && m_x+1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Gauche 1Haut
            else if(m_y+1 == y && m_x-2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Gauche 1Bas
            else if(m_y-1 == y && m_x-2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Bas 1Gauche
            else if(m_y-2 == y && m_x-1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Bas 1Droite
            else if(m_y-2 == y && m_x+1 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Droite 1Haut
            else if(m_y+1 == y && m_x+2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;

            //Mouvement 2Droite 1Bas
            else if(m_y-1 == y && m_x+2 == x && (e.getPiece(x,y) == 0 || e.getPiece(x, y)->estBlanc()))
                return true;
        }
    }
    return false;
}


