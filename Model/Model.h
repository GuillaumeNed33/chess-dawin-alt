#ifndef POO_DAWIN_ALT_MODEL_H
#define POO_DAWIN_ALT_MODEL_H


#include "JoueurBlanc.h"
#include "JoueurNoir.h"
#include <vector>

class Model {
private:
    JoueurBlanc m_joueurBlanc;
    JoueurNoir m_joueurNoir;
    Echiquier m_echiquier;

public:
    Model();
    std::vector<std::pair<int, int> > getPositions(bool white);
    Echiquier getEchiquier() const;
    JoueurBlanc getJoueurBlanc() const;
    JoueurNoir getJoueurNoir() const;
    std::vector<std::pair<int, int> >getPossibilities(int posX, int posY);
    bool deplacer(int posX, int posY, int destX, int destY);

};


#endif //POO_DAWIN_ALT_MODEL_H
