#include "Reine.h"
#include <iostream>
#include "Echiquier.h"

using namespace std;

/**
 * @brief Reine::Reine
 */
Reine::Reine()
{
   // cout << "Nouvelle Reine."<< endl;
}

/**
 * @brief Reine::Reine
 * @param white
 */
Reine::Reine(bool white) : Piece(4, (white?1:8), white), Fou(), Tour() {
  //  cout << "Nouvelle Reine en ( " << 4 << " , " << (white?1:8) << " )."<< endl;
}

/**
 * @brief Reine::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Reine::mouvementValide(Echiquier &e, int x, int y) {
   return Fou::mouvementValide(e, x, y) || Tour::mouvementValide(e, x, y);
}

/**
 * @brief Reine::type
 * @return Representation graphique de la piece (terminal)
 */
char
Reine::type()
{
  return m_white ? 'Q' : 'q';
}
