#include "Tour.h"
#include <iostream>
#include "Echiquier.h"

using namespace std;

/**
 * @brief Tour::Tour
 */
Tour::Tour()
{
    //cout << "Nouvelle Tour."<< endl;
}

/**
 * @brief Tour::Tour
 * @param white
 * @param gauche
 */
Tour::Tour(bool white, bool gauche) : Piece(gauche?1:8, white?1:8, white) {
   // int y = (white)?1:8;
   // int x = (gauche)?1:8;
   // cout << "Nouvelle Tour en ( " << x << " , " << y << " )."<< endl;
}

/**
 * @brief Tour::type
 * @return Representation graphique de la piece (terminal)
 */
char
Tour::type()
{
  return m_white ? 'T' : 't';
}

/**
 * @brief Tour::validMove
 * @param e Echiquier
 * @param x destination x
 * @param y destination y
 * @return possibilité de bouger la piece en x,y
 */
bool Tour::mouvementValide(Echiquier &e, int x, int y) {
    //Coordonnées valides
    if( (x >=1 && x <=8) && (y >=1 && y <=8))
    {
        //Blanc
        if(this->estBlanc()) {
            //Mouvement vertical
            if(m_x == x && m_y != y) {
                if(y > m_y) {
                    int cpt = m_y+1;
                    bool valid = true;
                    while(valid && cpt <= y) {
                        if(e.getPiece(x, cpt) != 0 && cpt != y)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estNoir())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estBlanc())
                        {
                            valid = false;
                        }
                        cpt++;
                    }
                    return valid;
                }
                else if(y < m_y){
                    int cpt = m_y-1;
                    bool valid = true;
                    while(valid && cpt >= y) {
                        if(e.getPiece(x, cpt) != 0 && cpt != y)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estNoir())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estBlanc())
                        {
                            valid = false;
                        }
                        cpt--;
                    }
                    return valid;
                }
            }

            //Mouvement horizontal
            else if(m_y == y && m_x != x) {
                if(x > m_x) {
                    int cpt = m_x+1;
                    bool valid = true;
                    while(valid && cpt <= x) {
                        if(e.getPiece(cpt, y) != 0 && cpt != x)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estNoir())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estBlanc())
                        {
                            valid = false;
                        }
                        cpt++;
                    }
                    return valid;
                }
                else if(x < m_x) {
                    int cpt = m_x-1;
                    bool valid = true;
                    while(valid && cpt >= x) {
                        if(e.getPiece(cpt, y) != 0 && cpt != x)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estNoir())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estBlanc())
                        {
                            valid = false;
                        }
                        cpt--;
                    }
                    return valid;
                }
            }
        }

        //noir
        else {
            //Mouvement vertical
            if(m_x == x && m_y != y) {
                if(y > m_y) {
                    int cpt = m_y+1;
                    bool valid = true;
                    while(valid && cpt <= y) {
                        if(e.getPiece(x, cpt) != 0 && cpt != y)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estBlanc())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estNoir())
                        {
                            valid = false;
                        }
                        cpt++;
                    }
                    return valid;
                }
                else {
                    int cpt = m_y-1;
                    bool valid = true;
                    while(valid && cpt >= y) {
                        if(e.getPiece(x, cpt) != 0 && cpt != y)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estBlanc())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(x, cpt) != 0 && cpt == y && e.getPiece(x, y)->estNoir())
                        {
                            valid = false;
                        }
                        cpt--;
                    }
                    return valid;
                }
            }

            //Mouvement horizontal
            else if(m_y == y && m_x != x) {
                if(x > m_x) {
                    int cpt = m_x+1;
                    bool valid = true;
                    while(valid && cpt <= x) {
                        if(e.getPiece(cpt, y) != 0 && cpt != x)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estBlanc())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estNoir())
                        {
                            valid = false;
                        }
                        cpt++;
                    }
                    return valid;
                }
                else {
                    int cpt = m_x-1;
                    bool valid = true;
                    while(valid && cpt >= x) {
                        if(e.getPiece(cpt, y) != 0 && cpt != x)
                        {
                            valid = false;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estBlanc())
                        {
                            valid = true;
                        }
                        else if(e.getPiece(cpt, y) != 0 && cpt == x && e.getPiece(x, y)->estNoir())
                        {
                            valid = false;
                        }
                        cpt--;
                    }
                    return valid;
                }
            }
        }
    }
    return false;
}
