/** 
 * Mise en oeuvre de Echiquier.h
 *
 * @file Echiquier.cxx
 */

#include <iostream>
// A besoin de la declaration de la classe
#include "Echiquier.h"
#include "Roi.h"
#include "Reine.h"
#include "Cavalier.h"
#include "Fou.h"
#include "Tour.h"
#include "Pion.h"



using namespace std;

/**
 * Constructeur par d�faut.
 * Initialise � vide l'echiquier.
 */
Echiquier::Echiquier()
{
    for(int i=0; i<64; i++) {
        m_cases[i] = nullptr;
    }
}


/**
 * Recupere la piece situee sur une case.
 *
 * @param x un entier entre 1 et 8
 * @param y un entier entre 1 et 8
 *
 * @return 0 si aucune piece n'est sur cette case et un pointeur
 * vers une piece sinon.
 */
Piece*
Echiquier::getPiece( int x, int y )
{
    if(m_cases[((y-1)*8)+x-1] != nullptr)
        return m_cases[((y-1)*8)+x-1];
    else
        return 0;
}


/**
 * Place une piece sur l'echiquier, aux coordonnees specifiees dans la piece.
 *
 * @param p un pointeur vers une piece
 *
 * @return 'true' si le placement s'est bien passe, 'false' sinon
 * (case occupee, coordonnees invalides, piece vide )
 */
bool
Echiquier::placer( Piece* p )
{
    if(p != nullptr) {
        if( (this->getPiece(p->x(), p->y()) == 0) &&
                (p->x() >= 1 && p->x() <= 8) &&
                (p->y() >= 1 && p->y() <= 8)){
            m_cases[((p->y()-1)*8)+p->x()-1] = p;
            return true;
        }
    }
    return false;
}


/**
 * Deplace une piece sur l'echiquier, des coordonnees specifiees
 * dans la piece aux coordonnees x,y.
 *
 * @param p un pointeur vers une piece
 * @param x un entier entre 1 et 8
 * @param y un entier entre 1 et 8
 *
 * @return 'true' si le placement s'est bien passe, 'false' sinon
 * (case occupee, coordonnees invalides, piece vide, piece pas
 * presente au bon endroit sur l'echiquier)
 */
bool
Echiquier::deplacer( Piece* p, int x, int y )
{
    if(p !=nullptr) {
        if(p->mouvementValide(*this, x, y))
        {
            if(!isCheck(p,x,y)) {
               enleverPiece(p->x(), p->y());
                p->move(x,y);
                Piece* detruite = enleverPiece(x,y);
                if(detruite != 0) {
                    Piece* test = dynamic_cast<Roi*>(detruite);
                    if(test != 0)
                        return false;

                    if(detruite->estBlanc())
                        m_deleted_white.push_back(detruite);
                    else
                        m_deleted_black.push_back(detruite);
                }
                placer(p);
                return true;
            }
        }
    }
    return false;
}


/**
 * Enleve la piece situee sur une case (qui devient vide).
 *
 * @param x un entier entre 1 et 8
 * @param y un entier entre 1 et 8
 *
 * @return 0 si aucune piece n'est sur cette case et le pointeur
 * vers la piece enlevee sinon.
 */
Piece*
Echiquier::enleverPiece( int x, int y )
{
    if ( ( x >= 1 ) && ( x <= 8 )
         && ( y >= 1 ) && ( y <= 8 )
         && ( getPiece( x, y ) != 0 ) )
    {
        if(getPiece(x,y) != 0) {
            Piece*p = m_cases[((y-1)*8)+x-1];
            m_cases[((y-1)*8)+x-1] = nullptr;
            return p;
        }
    }
    return 0;
}


/**
 * Affiche l'echiquier avec des # pour les cases noires et . pour
 * les blanches si elles sont vides, et avec B pour les pieces
 * blanches et N pour les pieces noires.
 */
void
Echiquier::affiche()
{
    cout << endl << "    1 2 3 4 5 6 7 8" << endl << endl;
    for ( int y = 1; y <= 8; ++y )
    {
        cout << y << "   ";
        for ( int x = 1; x <= 8; ++x )
        {
            char c;
            Piece* p = getPiece( x, y );
            if ( p == 0 )
                c = ( ( x + y ) % 2 ) == 0 ? '#' : '.';
            else {
                c = p->type();
            }
            cout << c << " ";
        }
        cout << "   " << y << endl;
    }
    cout << endl << "    1 2 3 4 5 6 7 8" << endl << endl;
}

/**
 * @brief Echiquier::isCheck
 * @param p Piece qui se deplace
 * @param x position x d'arrivée de la pièce p
 * @param y position x d'arrivée de la pièce p
 * @return regarde si après le deplacement de la piece p, son roi n'est pas en echec
 */
bool
Echiquier::isCheck(Piece *p, int x, int y) {

    int oldX= p->x();
    int oldY= p->y();


    enleverPiece(p->x(), p->y());
    p->move(x,y);
    Piece* detruite = enleverPiece(x,y);
    placer(p);

    bool enEchec = echec(p->estBlanc());

    //REPLACE LES PIECES
    enleverPiece(x,y);
    p->move(oldX,oldY);
    placer(p);
    placer(detruite);

    return enEchec;
}

/**
 * @brief Echiquier::echec
 * @param white Couleur du joueur
 * @return true si le roi de la couleur en parametre est en echec
 */
bool Echiquier::echec(bool white) {

    //ON REGARDE SI LE ROI EST EN ECHEC

    //recherche coordonnées du roi de la couleur oppose
    int i = 1;
    int j = 1;
    bool trouve = false;
    bool enEchec = false;

    while(!trouve && (i <= 8 && j <= 8)) {
        Piece* test = dynamic_cast<Roi*>(getPiece(i,j));
        if(test != 0 && test->estBlanc() == white) {
            trouve = true;
        }
        else {
            if(i==8) {
                i=1;
                j++;
            }
            else
                i++;
        }
    }

    //Test de echecs
    int u = 1;
    int w =1;

    while(!enEchec && (u <= 8 && w <= 8)) {

        if( getPiece(u,w) != 0 && (getPiece(u, w)->estBlanc() != white) && (getPiece(u, w)->mouvementValide(*this, i, j)) ) {
            enEchec = true;
        }
        else {
            if(u==8) {
                u=1;
                w++;
            }
            else
                u++;
        }
    }
    return enEchec;
}

/**
 * @brief Echiquier::EchecEtMat
 * @param blanc Couleur du joueur
 * @return regarde si le roi de la couleur en parametre est en echec et mat
 */
bool
Echiquier::EchecEtMat(bool blanc) { //true : regarde si les blancs sont en echcs et mat

    int i = 1;
    int j = 1;
    bool trouve = false;
    Piece* roi;

    //recherche du roi
    while(!trouve && (i <= 8 && j <= 8)) {
        roi = dynamic_cast<Roi*>(getPiece(i,j));
        if(roi != 0 && roi->estBlanc() == blanc) {
            trouve = true;
        }
        else {
            if(i==8) {
                i=1;
                j++;
            }
            else
                i++;
        }
    }

    //roi introuvable
    if(roi == 0)
        return false;

    //Roi peut il bouger ?
    int l = 1;
    int c = 1;
    bool mouvementBloque = true;
    while(mouvementBloque && (l <= 8 && c <= 8)) {
        if(roi->mouvementValide(*this, c, l) && !isCheck(roi, c,l)) {
            mouvementBloque = false;
        }
        else {
            if(c==8) {
                c=1;
                l++;
            }
            else {
                c++;
            }
        }
    }

    //si le roi ne peut pas bouger, on verifie si une autre piece peut le sauver
    if(mouvementBloque) {
        bool sauvetagePossible = false;
        Piece* p;
        i = 1;
        j = 1;
        while(!sauvetagePossible && (i <= 8 && j <= 8)) {
            if(getPiece(i,j) != 0 && getPiece(i, j)->estBlanc() == blanc) {
                p = getPiece(i,j);
                //Test de tous les deplacement possible pour sauver son roi
                bool sauverLeRoi = false;
                l = 1;
                c = 1;
                while(!sauverLeRoi && (l <= 8 && c <= 8)) {
                    if(p->mouvementValide(*this, c, l) && !isCheck(p, c,l)) {
                        sauverLeRoi = true;
                    }
                    else {
                        if(c==8) {
                            c=1;
                            l++;
                        }
                        else {
                            c++;
                        }
                    }
                }
                if(sauverLeRoi) {
                    sauvetagePossible = true;
                }
                else {
                    if(i==8) {
                        i=1;
                        j++;
                    }
                    else
                        i++;
                }
            }
            else {
                if(i==8) {
                    i=1;
                    j++;
                }
                else
                    i++;
            }
        }
        return !sauvetagePossible;
    }
    return false;
}

/**
 * @brief Echiquier::configCheckMate
 * Place les pieces de facon a ce que les noirs soient en echec et mat au tour suivant
 */
void Echiquier::configEchecEtMat() {
    //Reine
    Piece* p = enleverPiece(4,1);
    p->move(8,5);
    enleverPiece(8,5);
    placer(p);

    //Fou
    Piece* p2 = enleverPiece(6,1);
    p2->move(3,4);
    enleverPiece(3,4);
    placer(p2);

    //Pion noir
    Piece* p3 = enleverPiece(1,7);
    p3->move(1,5);
    enleverPiece(1,5);
    placer(p3);
}


/**
 * @brief Echiquier::PAT
 * @param blanc Couleur du joueur
 * @return regarde si la couleur en parametre est en PAT (aucun deplacement possible)
 */
bool
Echiquier::PAT(bool blanc) { //true : regarde si les blancs sont en situation de PAT

    bool PAT = true;

    if(echec(blanc)) {
        return false;
    }
    else {
        int i = 1;
        int j = 1;
        bool mvtPossible = false;
        Piece * p;

        //On cherche toutes les pieces de la couleur
        while(!mvtPossible && (i <= 8 && j <= 8)) {
            if(getPiece(i,j) != 0 && getPiece(i, j)->estBlanc() == blanc) {

                p = getPiece(i,j);
                bool movable = false;

                int l = 1;
                int c = 1;
                while(!movable && (l <= 8 && c <= 8)) {
                    if(p->mouvementValide(*this, c, l) && !isCheck(p, c,l)) {
                        movable = true;
                    }
                    else {
                        if(c==8) {
                            c=1;
                            l++;
                        }
                        else {
                            c++;
                        }
                    }
                }
                if(movable) {
                    mvtPossible = true;
                    PAT = false;
                }
                else {
                    if(i==8) {
                        i=1;
                        j++;
                    }
                    else
                        i++;
                }
            }
            else {
                if(i==8) {
                    i=1;
                    j++;
                }
                else
                    i++;
            }
        }
    }
    return PAT;
}

/**
 * @brief Echiquier::configCheckMate
 * Place les pieces de facon a ce que les blancs ne peuvent effectuer aucun deplacements
 */
void Echiquier::configPAT() {

    Piece* tourN;
    Piece* roiN;
    Piece* cavalN;
    Piece* pionN;

    Piece* fouB;
    Piece* pionB;
    Piece* pion2B;
    Piece* roiB;

    //On retire les pieces de l'echiquier (en gardant les piece interessante)
    for(int j=1; j<=8; j++) {
        for(int i=1; i<=8; i++) {

            if(i==1 && j==8)
                tourN = enleverPiece(i,j);

            else if(i==2 && j==8)
                cavalN = enleverPiece(i,j);

            else if(i==5 && j==8)
                roiN = enleverPiece(i,j);

            else if(i==1 && j==7)
                pionN = enleverPiece(i,j);

            else if(i==1 && j==2)
                pionB = enleverPiece(i,j);

            else if(i==2 && j==2)
                pion2B = enleverPiece(i,j);

            else if(i==3 && j==1)
                fouB = enleverPiece(i,j);

            else if(i==5 && j==1)
                roiB = enleverPiece(i,j);
            else
                enleverPiece(i,j);
        }
    }
    //Tour Noir
    tourN->move(8,1);
    placer(tourN);

    //Cavalier Noir
    cavalN->move(5,4);
    placer(cavalN);

    //Roi Noir
    roiN->move(6,3);
    placer(roiN);

    //Pion Noir
    pionN->move(7,7);
    placer(pionN);


    /***********/

    //Pion Blanc
    pionB->move(7,5);
    placer(pionB);

    //Pion Blanc 2
    pion2B->move(7,3);
    placer(pion2B);

    //Fou Blanc
    fouB->move(8,3);
    placer(fouB);

    //Roi Blanc
    roiB->move(8,4);
    placer(roiB);
}


bool Echiquier::isNul() {
    int cptPiece=0;
    int cptRoi =0;
    for(int j=1; j<=8; j++) {
        for(int i=1; i<=8; i++) {
            if(getPiece(i,j) != 0) {
                cptPiece++;
                Piece* test = dynamic_cast<Roi*>(getPiece(i,j));
                if(test != 0) {
                    cptRoi++;
                }
            }
        }
    }
    return (cptPiece==2 && cptRoi==2);
}

void Echiquier::configNul() {
    Piece* roiB;
    Piece* roiN;
    Piece* pionN;

    //On retire les pieces de l'echiquier (en gardant les piece interessante)
    for(int j=1; j<=8; j++) {
        for(int i=1; i<=8; i++) {

            if(i==5 && j==1)
                roiB = enleverPiece(i,j);

            else if(i==5 && j==8)
                roiN = enleverPiece(i,j);

            else if(i==1 && j==7)
                pionN = enleverPiece(i,j);
            else
                enleverPiece(i,j);
        }
    }

    //Pion Noir
    pionN->move(3,4);
    placer(pionN);

    //Roi Noir
    roiN->move(6,7);
    placer(roiN);

    //Roi Blanc
    roiB->move(2,4);
    placer(roiB);
}
