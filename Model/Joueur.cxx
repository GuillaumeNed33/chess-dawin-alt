#include <iostream>
#include "Joueur.h"
#include "Roi.h"
#include "Reine.h"
#include "Tour.h"
#include "Fou.h"
#include "Cavalier.h"
#include "Pion.h"

using namespace std;

/**
 * @brief Joueur::Joueur
 */
Joueur::Joueur()
{
   // cout << "Création de joueur sans parametres " << endl;
}

/**
 * @brief Joueur::Joueur
 * @param white couleur joueur
 */
Joueur::Joueur(bool white)
{
    init(white);
}

/**
 * @brief Joueur::~Joueur
 */
Joueur::~Joueur()
{
    for(Piece* p : m_pieces)
        delete p;

    m_pieces.clear();
}

/**
 * @brief Joueur::init
 * @param white
 * initie toutes les pieces du joueur
 */
void
Joueur::init(bool white)
{  
    for (int i=1; i<=8; i++)
    {
        m_pieces.push_back(new Pion(i,white));
    }
    m_pieces.push_back(new Roi(white));
    m_pieces.push_back(new Reine(white));
    m_pieces.push_back(new Cavalier(white, true));
    m_pieces.push_back(new Cavalier(white, false));
    m_pieces.push_back(new Fou(white, true));
    m_pieces.push_back(new Fou(white, false));
    m_pieces.push_back(new Tour(white, true));
    m_pieces.push_back(new Tour(white, false));
}

/**
 * @brief Joueur::affiche
 * affiche les pieces du joueur
 */
void
Joueur::affiche() const
{
    for(Piece* p : m_pieces)
        p->affiche();
}

/**
 * @brief Joueur::placerPieces
 * @param e
 * @return true si toutes les pieces ont bien été placées sur l'echiquier
 */
bool
Joueur::placerPieces(Echiquier & e)
{
    bool returned = true;
    for (int i=0; i < 16; i++)
        returned &= e.placer(m_pieces.at(i));

    return returned;
}

/**
 * @brief Joueur::samePosition
 * @param p1
 * @param p2
 * @return si 2 pièces sont positionnées au meme endroit sur l'echiquier
 */
bool
Joueur::memePosition(const Piece *p1, const Piece *p2) const {
    return (p1->x() == p2->x() && p1->y() == p2->y());
}

std::vector<std::pair<int, int> > Joueur::getPositions() {
    vector<pair<int, int>> pos;
    for(Piece *p : m_pieces) {
        pos.push_back(make_pair(p->x(),p->y()));
    }
    return pos;
}

