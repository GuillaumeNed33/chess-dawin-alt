#if !defined Joueur_h
#define Joueur_h

#include "Piece.h"
#include "Echiquier.h"
#include <vector>

class Joueur
{
protected:
    std::vector<Piece *>m_pieces;

public:
    Joueur( bool white );
    Joueur();
    virtual ~Joueur();

    void init( bool white );
    void affiche() const;
    bool placerPieces(Echiquier & e);
    bool memePosition(const Piece *p1, const Piece *p2) const;
    std::vector<std::pair<int, int> > getPositions();
};

#endif // !defined Joueur_h
